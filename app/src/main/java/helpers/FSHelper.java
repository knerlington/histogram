package helpers;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * Created by danlakss on 12/10/15.
 * Make sure write permission is added in manifest if you want to write to external storage
 * Internal storage doesn't need any permissions
 * Since android 4.4 permissions aren't needed for private app dir in external storage
 */
public class FSHelper {


    public FSHelper(){

    }

    public boolean isExternalStorageMounted(){
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)){
            Log.d("fshelper","Ext storage mounted");
            return true;
        }else{
            Log.d("fshelper","Ext storage NOT mounted");
            return false;
        }



    }

    //returns true if ext storage is present, but NOT mounted and instead shared via USB
    public boolean isExternalStorageShared(){
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_SHARED.equals(state)){
            Log.d("fshelper","Ext storage shared via USB, NOT MOUNTED");
            return true;
        }else{
            Log.d("fshelper","Ext storage not shared");
            return false;
        }
    }

    //returns primary private external storage dir
    public String printPrimaryExtPrivateStorageDirectory(Context context) {


        return context.getExternalFilesDir(null).getPath();
    }

    //returns primary public external storage directory
    //May be external space on sd card or on internal storage
    public void printPrimaryExtStorageDirectory(){
        File dir = Environment.getExternalStorageDirectory();
        Log.d("fshelper","primary ext storage dir: " +dir.getPath());
    }
    //creates subDir in one of the public constant directories such as DIRECTORY_DOWNLOADS
    public void createDirInPreDefinedPrimaryExtStorage(String publicDir, String subDir){
        File file = new File(Environment.getExternalStoragePublicDirectory(publicDir),subDir);
        if(!file.mkdir()){
            Log.d("FSHelper: ", "Couldn't create dir in specified path.");
        }
    }

    //assumes you're saving a file in a dir located in the public external root storage
    public File createFileInCustomDir(String dir, String name){
        File rootPath = Environment.getExternalStorageDirectory();
        String strPath = rootPath.getPath() + "/" +dir;
        Log.d("fshelper", "path for file: " + strPath);
        File file = new File(strPath, name);

        try{
            if(!file.exists()){
                file.createNewFile();
                Log.d("fshelper", "file with name: "+name+"created");
            }
        }catch (IOException e){
            Log.d("fshelper", e.getMessage());
        }
        return file;
    }

    public void createDirInPublicPrimaryExtRootStorage(String subDir){
        //Environment.getExternalStorageDirectory() returns the primary public external storage directory.
        File file = new File(Environment.getExternalStorageDirectory(),subDir);
        if(!file.exists()){
            //file.mkdir();

            if(!file.mkdir()){
                Log.d("FSHelper: ", "Couldn't create dir in ext root path.");
            }else{
                Log.d("fshelper", "dir: "+file.getPath() +" created.");
            }
        }else if(file.exists()){
            Log.d("fshelper", "dir: "+file.getPath() +" already exist.");
        }


    }

    public File[] getFilesInDir(String dir){
        File file = new File(Environment.getExternalStorageDirectory(),dir);
        return file.listFiles();
    }
    public void printFilesInDir(String dir){
        File file = new File(Environment.getExternalStorageDirectory(),dir);
        File fileList[] = file.listFiles();
        if(fileList.length < 1){
            Log.d("file", "sorry no files");
        }else{
            for (File f:fileList){
                Log.d("file", f.getName());
            }
        }

    }
}
