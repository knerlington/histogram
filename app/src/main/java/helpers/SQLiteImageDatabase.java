package helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.knerlington.histogram.BuildConfig;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by knerlington on 2015-11-18.
 */
public class SQLiteImageDatabase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "database.db";
    private static final int DATABASE_VERSION = 3;
    private static final String TABLE_NAME = "imageIndex";
    private static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, R TEXT,G TEXT,B TEXT)";

    public SQLiteImageDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase db){
        //do stuff
        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /*
    * Inserts data into the database created in the constructor
    * */
    public void insertData(String name, String r, String g, String b){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("NAME",name);
        content.put("R", r);
        content.put("G", g);
        content.put("B", b);
        db.insert(TABLE_NAME, null, content);
        db.close();


    }
    /*
    * Checks for existing record in database
    * */
    public boolean hasRecord(String name){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = null;
        try{
            String query = "select NAME from " + TABLE_NAME+ " where " + "NAME" + "="  +"'" +name+"'";
            cursor = db.rawQuery(query,null);
            if(cursor != null){
                cursor.close();
                db.close();
                return true;
            }
            cursor.close();
            db.close();
            return false;
        }catch (Exception e){
            cursor.close();
            db.close();
            return false;
        }



    }

    public ArrayList<String> getImageFeaturesFromRecord(String imageName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select R,G,B from " + TABLE_NAME+ " where " + "NAME" + "="  +"'" +imageName+"'";
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<String> features = new ArrayList();
        while(cursor.moveToNext()){
            features.add(cursor.getString(cursor.getColumnIndex("R")));
            features.add(cursor.getString(cursor.getColumnIndex("G")));
            features.add(cursor.getString(cursor.getColumnIndex("B")));
        }


        return features;
    }


}
