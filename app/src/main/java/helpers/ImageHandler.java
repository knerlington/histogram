package helpers;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;

import com.knerlington.histogram.R;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import models.Histogram;
import models.MyImage;

/**
 * NOTES:
 *
 * 
 * Helper class for creating bitmaps from file, resources etc.
 * Can create pixel arrays from bitmaps for easier manipulation
 * Created by knerlington on 2015-10-27.
 */
public class ImageHandler {
    //file system related fields
    private FSHelper fsHelper; //File system helper
    private final String appDir = "Histogram";
    private final String fileNameRed = "redValues";
    private File currentFile;

    public File getCurrentFile() {
        return currentFile;
    }

    public String getFileNameRed() {
        return fileNameRed;
    }

    public FSHelper getFsHelper() {
        return fsHelper;
    }

    private final String fileNameGreen = "greenValues";
    private final String fileNameBlue = "blueValues";

    //images
    private ArrayList<MyImage> images = new ArrayList<>();
    private ArrayList<MyImage> filteredImages = new ArrayList<>();
    private MyImage currentImage;

    public void setCurrentImage(MyImage currentImage) {
        this.currentImage = currentImage;
    }

    public ArrayList<MyImage> getFilteredImages() {
        return filteredImages;
    }

    public void setFilteredImages(ArrayList<MyImage> filteredImages) {
        this.filteredImages = filteredImages;
    }

    public ArrayList<MyImage> getImages() {
        return images;
    }

    public MyImage getCurrentImage() {
        return currentImage;
    }

    public void createImage(Resources res, int id, Context context){
        MyImage img = new MyImage(res, id, context);
        this.images.add(img);
        this.currentImage = img;
    }
    public void createImageFromFile(File file, Context context) {
        MyImage img = new MyImage(file,context);
        this.images.add(img);
    }
    public void createImageUsingBitmap(Bitmap bitmap, Context context) {
        MyImage img = new MyImage(bitmap,context);
        this.images.add(img);
    }

    public boolean compareImagesUsingDistance(float distance, float offsetVal){
        float offset = offsetVal*distance;
        if(distance <= distance+offset && distance >= distance-offset){
            //if the second image is within the interval based on the first image distance
            //the image is similar
            return true;
        }
        return false;
    }

    /*
    * Calculates the Euclidean distance between two images
    * Returns the result as a string
    * Use this string for the DISTANCE column in the SQLite database*/
    public double calcEuclideanDistanceBetweenImages(MyImage image1, MyImage image2){
        //get mean values for each color component from the image histogram
        double rMean1 = image1.getHistogram().getrMean(), gMean1 = image1.getHistogram().getgMean(), bMean1 = image1.getHistogram().getbMean();
        double rMean2 = image2.getHistogram().getrMean(), gMean2 = image2.getHistogram().getgMean(), bMean2 = image2.getHistogram().getbMean();

        //calc Euclidean distance
        double result = Math.sqrt(Math.pow((rMean1-rMean2), 2)+Math.pow((gMean1-gMean2), 2)+Math.pow((bMean1-bMean2), 2));
        //String resultString = String.valueOf(result);

        return result;
    }

    public boolean compareImages(MyImage image1, MyImage image2, float offsetVal){
        //get mean val for each color channel for first image
        int rMean = image1.getHistogram().getrMean();
        int gMean = image1.getHistogram().getgMean();
        int bMean = image1.getHistogram().getbMean();
        int meanSum1 = (rMean+gMean+bMean)/3;

        //get mean val for second image
        int rMean2 = image2.getHistogram().getrMean();
        int gMean2 = image2.getHistogram().getgMean();
        int bMean2 = image2.getHistogram().getbMean();
        int meanSum2 = (rMean2+gMean2+bMean2)/3;

        float offset = offsetVal*meanSum1;
        Log.d("handler","image1 mean: "+meanSum1+" image2 mean: "+meanSum2 +" offset: "+offset);
        if(meanSum2 <= meanSum1+offset && meanSum2 >= meanSum1-offset){
            //if condition met show image
            Log.d("handler", "image is similar");
            image2.setIsSimilar(true);
            return true;
        }
        Log.d("handler", "image is NOT similar");
        return false;

    }


    private static ImageHandler ourInstance = new ImageHandler();

    public static ImageHandler getInstance() {
        return ourInstance;
    }

    private Bitmap currentBitmap;

    public Bitmap getCurrentBitmap() {
        return currentBitmap;
    }

    public void setCurrentBitmap(Bitmap currentBitmap) {
        this.currentBitmap = currentBitmap;
    }

    private ImageHandler() {
        fsHelper = new FSHelper();
        fsHelper.createDirInPublicPrimaryExtRootStorage(this.appDir);
    }


    public void createFileFromHistogram(String fileName){
        this.currentFile = this.fsHelper.createFileInCustomDir(this.appDir,fileName+".csv");
    }
    /*public void writeOccurrencesToFile(File file){
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, true *//*append*//*));

            writer.append("occurrence"); //write column names
            writer.append(",");
            writer.append("value");
            writer.append("\n");
            //check each hashmap entry
            //this way seems to be the only way to retrieve the key and value from hashmap
            for(Map.Entry<Integer, Integer> e:this.getCurrentHistogram().getNumOccurence().entrySet()){
                int key = e.getKey();
                int value = e.getValue();



                writer.append(Integer.toString(value)); //write occurrence
                writer.append(",");
                writer.append(Integer.toString(key)); //write key (the component int value)
                writer.append("\n");
            }
            writer.close();
        }catch(IOException e){

        }
    }*/




    //decodes a resource drawable and returns it as a bitmap
    //use the bitmap to loop through the pixels in the image
    public Bitmap createBitmapFromResourceId(Resources res, int id){
        //Options used in decodeResources() makes sure that the image isn't scaled
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inScaled = false;
        Bitmap bm = BitmapFactory.decodeResource(res, id, bitmapOptions);
        return bm;
    }

   /* //creates and returns an array with the colors for all pixels in the bitmap
    public int[] createPixelArrayFromBitmap(Bitmap bm){
        this.setCurrentPixels(new int[bm.getWidth()*bm.getHeight()]);
        bm.getPixels(this.getCurrentPixels(),0,bm.getWidth(),0, 0, bm.getWidth(), bm.getHeight());
        return this.getCurrentPixels();
    }

    public void getRGBFromPixels(int[] pixels){
        redValues = new ArrayList<Integer>();
        greenValues = new ArrayList<Integer>();
        blueValues = new ArrayList<Integer>();

        for(int i = 0; i < pixels.length;i++){
            int red = Color.red(pixels[i]);
            int green = Color.green(pixels[i]);
            int blue = Color.blue(pixels[i]);

            //add each pixels color values for each channel to array
            redValues.add(red);
            greenValues.add(green);
            blueValues.add(blue);

            //Log.d("ImageHandler", "colorInt as R: "+red+"G: "+green+"B: "+blue);

        }

    }

    public void sortRGBValues(){
        Collections.sort(redValues);
        Collections.sort(greenValues);
        Collections.sort(blueValues);
        Log.d("imagehandler", "red values: " + redValues);
        Log.d("imagehandler", "green values: "+greenValues);
        Log.d("imagehandler", "blue values: "+blueValues);
    }*/
}
