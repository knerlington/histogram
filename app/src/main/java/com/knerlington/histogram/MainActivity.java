package com.knerlington.histogram;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.File;
import java.util.Map;

import helpers.ImageHandler;
import helpers.SQLiteImageDatabase;
import models.Histogram;

public class MainActivity extends AppCompatActivity {
    private ImageHandler imageHandler;
    private SQLiteImageDatabase imageDB;
    private int[] pixels;
    private Bitmap bm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = (Button) findViewById(R.id.button_color);

        //Image handling
        imageHandler = ImageHandler.getInstance();
        imageHandler.getFsHelper().printFilesInDir("Histogram");
        File fileList[] = imageHandler.getFsHelper().getFilesInDir("Histogram");
        for(File f:fileList){
            imageHandler.createImageFromFile(f,this);
            Log.d("start", "image name: "+f.getName());
        }

        //get display dimensions
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);


        RelativeLayout rl = (RelativeLayout)findViewById(R.id.rootLayout);
    }

    public void buttonMethod(View v){
        switch (v.getId()){
            case R.id.button_color:
               /* for(int i: this.pixels){
                    v.setBackgroundColor(i);
                    Log.d("main", "Color on button: "+i);
                }*/
               Intent intent = new Intent(this, GalleryActivity.class);
                startActivity(intent);

                break;
        }


    }


}
