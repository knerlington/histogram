package com.knerlington.histogram;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;



import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import helpers.ImageHandler;
import models.ImageAdapter;
import models.MyImage;



public class GalleryActivity extends AppCompatActivity {
    private ImageHandler imageHandler = ImageHandler.getInstance();
    private GridView gv;
    private ImageAdapter iAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        this.gv = (GridView)findViewById(R.id.gridContainer);
        gv.setAdapter(new ImageAdapter());
        iAdapter = (ImageAdapter)gv.getAdapter();
        setClickListener();

        /*Http request
        * Executes an AsyncTask ImageDownloader defined further down
        * */

        /*final String imgUrl = "http://loremflickr.com/320/240";
        ArrayList<String> stringUrls = new ArrayList<>();
        ArrayList<URL> urls = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            stringUrls.add(imgUrl);
        }
        try{
            for(String s:stringUrls){
                URL url = new URL(s);
                ImageDownloader id = new ImageDownloader();
                id.execute(url);
            }


        }catch (IOException e){

        }*/
    }

    /*
    * Event handling
    * Marks items in grid
    * Starts new activity
    * */
    public void setClickListener(){
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Toast.makeText(GalleryActivity.this, "item clicked", Toast.LENGTH_SHORT).show();
                //Intent intent = new Intent(GalleryActivity.this, DetailActivity.class);
                //intent.putExtra("imageID", position);


                //set current image to selected item
                imageHandler.setCurrentImage(imageHandler.getImages().get(position));
                //mark item with different color
                imageHandler.getCurrentImage().setSelection();
                if (imageHandler.getCurrentImage().isSelected()) {
                    imageHandler.getCurrentImage().getImageView().setBackgroundColor(Color.CYAN);
                    imageHandler.getCurrentImage().getImageView().setImageAlpha(155);
                } else {

                    imageHandler.getCurrentImage().getImageView().setImageAlpha(255);
                }


            }
        });

    }
    /*Starts DetailsActivity if an item has been marked*/
    public void filterFromSelection(View v){
        if(imageHandler.getCurrentImage().isSelected()){
            Intent intent = new Intent(GalleryActivity.this, DetailActivity.class);
            startActivity(intent);
        }

    }

    /*AsyncTask
    * ImageDownloader
    * Pulls data from loremflickr
    * URL is in onCreate
    * */
    private class ImageDownloader extends AsyncTask<URL, Integer, Bitmap>{
        //HttpRequest, connection, stream conversion
        private Bitmap downLoadImageFromUrl(URL url){
            HttpURLConnection connection = null;
            try{
                connection = (HttpURLConnection)url.openConnection();
                int responseCode = connection.getResponseCode();
                Log.d("imagedownloader", "response code: "+responseCode);
               /* if(responseCode != HttpURLConnection.HTTP_ACCEPTED){
                //check response codes first!
                    return null;
                }*/

                InputStream is = connection.getInputStream();


                if(is != null){
                    //stream is good, return bitmap
                    Bitmap bm = BitmapFactory.decodeStream(is);
                    Log.d("ImageDownloader", "Bitmap from stream: "+bm.toString());
                    return bm;
                }
            }catch (Exception e){
                //in case of errors while fetching image
                Log.d("ImageDownloader", "Error fetching image from URL: "+url);
            }finally {
                //close the connection etc.
                connection.disconnect();
            }
            return null;
        }
        @Override
        protected void onPreExecute (){
            //runs on ui thread before doInBackground
            Toast.makeText(GalleryActivity.this, "Downloading images...", Toast.LENGTH_SHORT).show();
        }
        @Override
        protected Bitmap doInBackground(URL... params){

            return downLoadImageFromUrl(params[0]);
        }
        @Override
        protected void onPostExecute(Bitmap result) {

            Log.d("ImageDownloader", "onPostExecute");
            Log.d("ImageDownloader", "Bitmap from stream: "+result.toString());
            if(result != null){
                imageHandler.createImageUsingBitmap(result,GalleryActivity.this);
                Log.d("ImageDownloader", "Bitmap dimensions: "+result.getWidth()+" "+result.getHeight());
            }

            iAdapter.notifyDataSetChanged();
        }


    }
}


