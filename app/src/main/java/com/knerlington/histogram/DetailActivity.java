package com.knerlington.histogram;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

import helpers.ImageHandler;
import helpers.SQLiteImageDatabase;
import models.Histogram;
import models.MyImage;

public class DetailActivity extends AppCompatActivity {
    private ImageHandler imageHandler = ImageHandler.getInstance();
    private SQLiteImageDatabase database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ImageView iv = (ImageView)findViewById(R.id.imageView_placeholder);
        GridView grid = (GridView)findViewById(R.id.gridView);
        database = new SQLiteImageDatabase(DetailActivity.this);
        iv.setImageBitmap(imageHandler.getCurrentImage().getBitmap());
        Log.d("images", "nr of images " + imageHandler.getImages().size());

        //AsyncTask
        ImageComparator comparator = new ImageComparator();
        comparator.execute(imageHandler.getImages().toArray(new MyImage[imageHandler.getImages().size()]));
    }

    private class ImageComparator extends AsyncTask<MyImage, Integer, ArrayList<Double>>{
        private SQLiteImageDatabase database;
        private ArrayList<Double> results;
        @Override
        protected void onPreExecute(){
            //prepare database
            database = DetailActivity.this.database;
            results = new ArrayList<>();
        }

        @Override
        protected ArrayList<Double> doInBackground(MyImage... params) {

            Log.d("doInBack", "doInBack reached");
            for(MyImage img:params){
                //create pixelArray & save each color component value
                imageHandler.getCurrentImage().createPixelArrayFromBitmap();
                imageHandler.getCurrentImage().getRGBFromPixels(imageHandler.getCurrentImage().getPixels());
                //calc occurrence of each value
                imageHandler.getCurrentImage().getHistogram().calcOccurrenceOfValues(imageHandler.getCurrentImage().getrValues(), Histogram.colorComponent.RED);
                imageHandler.getCurrentImage().getHistogram().calcOccurrenceOfValues(imageHandler.getCurrentImage().getgValues(), Histogram.colorComponent.GREEN);
                imageHandler.getCurrentImage().getHistogram().calcOccurrenceOfValues(imageHandler.getCurrentImage().getbValues(), Histogram.colorComponent.BLUE);
                //calc mean val from occurrences
                imageHandler.getCurrentImage().getHistogram().calcMeanValueFromOccurrences(imageHandler.getCurrentImage().getHistogram().getrOccurrence(), Histogram.colorComponent.RED);
                imageHandler.getCurrentImage().getHistogram().calcMeanValueFromOccurrences(imageHandler.getCurrentImage().getHistogram().getgOccurrence(), Histogram.colorComponent.GREEN);
                imageHandler.getCurrentImage().getHistogram().calcMeanValueFromOccurrences(imageHandler.getCurrentImage().getHistogram().getbOccurrence(), Histogram.colorComponent.BLUE);
                Log.d("mean", "currentImage mean r: "+String.valueOf(imageHandler.getCurrentImage().getHistogram().getrMean()));
                Log.d("mean", "currentImage mean g: "+String.valueOf(imageHandler.getCurrentImage().getHistogram().getrMean()));
                Log.d("mean","currentImage mean b: "+String.valueOf(imageHandler.getCurrentImage().getHistogram().getrMean()));

                //add as record and calc image features
                img.createPixelArrayFromBitmap();
                img.getRGBFromPixels(img.getPixels());
                img.getHistogram().calcOccurrenceOfValues(img.getrValues(), Histogram.colorComponent.RED);
                img.getHistogram().calcOccurrenceOfValues(img.getgValues(), Histogram.colorComponent.GREEN);
                img.getHistogram().calcOccurrenceOfValues(img.getbValues(), Histogram.colorComponent.BLUE);

                img.getHistogram().calcMeanValueFromOccurrences(img.getHistogram().getrOccurrence(), Histogram.colorComponent.RED);
                img.getHistogram().calcMeanValueFromOccurrences(img.getHistogram().getgOccurrence(), Histogram.colorComponent.GREEN);
                img.getHistogram().calcMeanValueFromOccurrences(img.getHistogram().getbOccurrence(), Histogram.colorComponent.BLUE);


                //compare images
                double result = imageHandler.calcEuclideanDistanceBetweenImages(imageHandler.getCurrentImage(), img);
                results.add(result);
            }
            

            return results;
        }
        @Override
        protected void onPostExecute (ArrayList<Double> result){
            for(double d:result){
                Log.d("compare","distance after comparison: "+d);
            }

        }


    }
}
