package models;

import android.graphics.Bitmap;
import android.media.Image;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filterable;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

import helpers.ImageHandler;

/**
 * NOTES:
 *
 * Adapter used to provide GridView in GalleryActivity with data
 * Created by knerlington on 2015-11-06.
 */
public class ImageAdapter extends BaseAdapter {
    private ImageHandler imageHandler = ImageHandler.getInstance();

    @Override
    public int getCount() {
        return imageHandler.getImages().size();
    }

    @Override
    public Object getItem(int position) {
        return imageHandler.getImages().get(position);

    }

    @Override
    public long getItemId(int position) {


        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView iv;
        iv = imageHandler.getImages().get(position).getImageView();
        iv.setLayoutParams(new GridView.LayoutParams(320, 240));
        iv.setScaleType(ImageView.ScaleType.CENTER_CROP);

        return iv;
    }


}
