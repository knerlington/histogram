package models;

import android.util.Log;
import android.util.Pair;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * NOTES:
 *
 * Created by danlakss on 28/10/15.
 */
public class Histogram {

    private final int colorInterval = 255;
    private int rMean, gMean, bMean;

    public int getrMean() {
        return rMean;
    }

    public int getgMean() {
        return gMean;
    }

    public int getbMean() {
        return bMean;
    }

    private Map<Integer, Integer> numOccurence = new HashMap<Integer, Integer>();

    private Map<Integer, Integer> rOccurrence = new HashMap<Integer, Integer>(), gOccurrence = new HashMap<Integer, Integer>(), bOccurrence = new HashMap<Integer, Integer>();

    public Map<Integer, Integer> getrOccurrence() {
        return rOccurrence;
    }

    public Map<Integer, Integer> getbOccurrence() {
        return bOccurrence;
    }

    public Map<Integer, Integer> getgOccurrence() {
        return gOccurrence;
    }

    public enum colorComponent{
        RED, GREEN, BLUE
    }

    public Histogram(){


    }

    /*
    * Calculates the occurrence of every value for the passed color component
    * I.E. If you pass an arraylist with red values you save the occurrences of each red value in the rOccurrence hashmap*/
    public void calcOccurrenceOfValues(ArrayList<Integer> values, colorComponent component){
        Map<Integer, Integer> map = new HashMap<>();
        int counter = 0;
        switch (component){
            case RED:
                //add to red occurrence hashmap
                map = rOccurrence;
                break;
            case GREEN:
                //add to green occurrence hashmap
                map = gOccurrence;
                break;
            case BLUE:
                //add to blue occurrence hashmap
                map = bOccurrence;
                break;
        }
        for(int i = 0; i < values.size();i++){
           counter = 1;


            if(map.containsKey(values.get(i))){
                counter = map.get(values.get(i));
                counter++;
            }
            map.put(values.get(i), counter);




        }
    }



    /*DEPRECATED - Not very precise
    * returns the intensity for the passed hashmap as a string
    * the method counts how many values fall under respective category
    * the category with the most occurrences equals the final intensity*/
    public String determineIntensity(Map<Integer, Integer> occurrences){
        int vdark = 0, dark = 0, light = 0, vlight = 0;
        //check each entry in Map<>
        //if entry has key(color value) in range add to num of corresponding category
        for (Map.Entry<Integer, Integer> e:occurrences.entrySet()){
            if(e.getKey() >= 0 && e.getKey() <=63){
                //very dark
                vdark++;
            }else if(e.getKey() >= 64 && e.getKey() <=127){
                //dark
                dark++;
            }else if(e.getKey() >= 128 && e.getKey() <=191){
                //light
                light++;
            }else if(e.getKey() >= 192 && e.getKey() <=255){
                //very light
                vlight++;
            }
        }
        Log.d("histo", "vdark :"+vdark+" dark: "+dark+" light: "+light+" vlight: "+vlight);
        //save each category numbers in array
        //create new hashmap so I know which val is which
        Map<String, Integer> map = new HashMap<>();
        map.put("vdark", vdark);
        map.put("dark", dark);
        map.put("light", light);
        map.put("vlight", vlight);

        int highestVal = 0;
        String key = "";
        //check highest value in array
        //highest value in intensities[] equals nature of color intensity
        for(Map.Entry<String, Integer> e:map.entrySet()){
            if(e.getValue() > highestVal){
                highestVal = e.getValue();
                key = e.getKey();
            }

        }
        Log.d("histo", "Highest val: "+highestVal +" key: "+key);

        return key;

    }


    /*
    * */
    public int calcMeanValueFromOccurrences(Map<Integer, Integer> occurrences, Histogram.colorComponent component){
        //image characteristics defined using mean val
        int sum = 0;
        int numVal = 0; //nr of values
        for(Map.Entry<Integer, Integer> e:occurrences.entrySet()){
            sum += e.getKey();
            numVal++;
        }
        Log.d("histo", "sum of all color values: "+sum+" nr of values: "+numVal+" mean value: "+sum/numVal);
        int result = sum/numVal;
        switch (component){
            case RED:
                rMean = result;
                break;
            case GREEN:
                gMean = result;
                break;
            case BLUE:
                bMean = result;
                break;

        }
        return result;
    }


}
