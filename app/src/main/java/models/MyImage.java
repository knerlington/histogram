package models;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * NOTES: Every image should have an actual image.
 * Bitmap only has pixel data.
 * Resource reference? ImageView with background set via resource? ImageURL?
 * Create imageView at instatiation or when needed?
 * Create and compare bitmaps first? If similar create imageviews?
 *
 * Created by knerlington on 2015-11-02.
 */
public class MyImage {
    private String name;
    private Bitmap bitmap;
    private ImageView imageView;
    private ArrayList<Integer> rValues, gValues, bValues; //RGB component values (0-255)
    private int[] pixels; //Array to copy pixels from bitmap
    private Histogram histogram; //Histogram holding occurrences of all values for each color component
    private boolean isSimilar; //Set through ImageHandler in compare()

    //selection state
    private final int defaultAlpha = 255;
    private final int selectedAlpha = 155;
    private boolean isSelected = false;

    public boolean isSelected(){


        return isSelected;
    }
    /*Sets isSelected on/off*/
    public void setSelection(){
        if(isSelected){
            isSelected = false;
        }else{
            isSelected = true;
        }

    }

    public void setIsSimilar(boolean isSimilar) {
        this.isSimilar = isSimilar;
    }

    public boolean isSimilar() {
        return isSimilar;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public ArrayList<Integer> getrValues() {
        return rValues;
    }

    public ArrayList<Integer> getbValues() {
        return bValues;
    }

    public ArrayList<Integer> getgValues() {
        return gValues;
    }

    public int[] getPixels() {
        return pixels;
    }

    public Histogram getHistogram() {
        return histogram;
    }

    public MyImage(Resources res, int id, Context context){
        this.bitmap = createBitmapFromResourceId(res, id); //Decodes a resource as bitmap
        createPixelArrayFromBitmap(); //Creates pixel array from bitmap
        getRGBFromPixels(this.pixels); //Gets color values from pixels
        this.histogram = new Histogram();
        this.imageView = new ImageView(context);
        this.imageView.setImageResource(id);
    }

    public MyImage(File file, Context context){
        this.bitmap = BitmapFactory.decodeFile(file.getPath());
        this.imageView = new ImageView(context);
        this.imageView.setImageBitmap(this.bitmap);
        this.name = file.getName();
        this.histogram = new Histogram();
    }
    //Non-default constructor
    //Takes bitmap directly as param -
    //intead of reasource
    public MyImage(Bitmap bitmap, Context context){
        this.bitmap = bitmap;

        //createPixelArrayFromBitmap(); //Creates pixel array from bitmap
        //getRGBFromPixels(this.pixels); //Gets color values from pixels
        //this.histogram = new Histogram();
        this.imageView = new ImageView(context);
        this.imageView.setImageBitmap(this.bitmap);
    }

    //decodes a resource drawable and returns it as a bitmap
    //use the bitmap to loop through the pixels in the image
    public Bitmap createBitmapFromResourceId(Resources res, int id){
        //Options used in decodeResources() makes sure that the image isn't scaled
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inScaled = false;
        Bitmap bm = BitmapFactory.decodeResource(res, id, bitmapOptions);
        return bm;
    }

    public String getName() {
        return name;
    }

    public void getRGBFromPixels(int[] pixels){
        this.rValues = new ArrayList<Integer>();
        this.gValues = new ArrayList<Integer>();
        this.bValues = new ArrayList<Integer>();

        for(int i = 0; i < pixels.length;i++){
            int red = Color.red(pixels[i]);
            int green = Color.green(pixels[i]);
            int blue = Color.blue(pixels[i]);

            //add each pixels color values for each channel to array
            this.rValues.add(red);
            this.gValues.add(green);
            this.bValues.add(blue);

            //Log.d("ImageHandler", "colorInt as R: "+red+"G: "+green+"B: "+blue);

        }
        sortRGBValues();

    }

    //creates and returns an array with the colors for all pixels in the bitmap
    //NOTE: Doesn't actually return an array. getPixels() copies pixel data to passed array
    public void createPixelArrayFromBitmap(){

            this.pixels = new int[this.bitmap.getWidth()*this.bitmap.getHeight()];
            this.bitmap.getPixels(this.pixels, 0, this.bitmap.getWidth(), 0, 0, this.bitmap.getWidth(), this.bitmap.getHeight());




    }
    public void sortRGBValues(){
        Collections.sort(rValues);
        Collections.sort(gValues);
        Collections.sort(bValues);
    }
}
